# If not running interactivly don't do anything
# This happens when running scripts
[[ $- != *i* ]] && return

# search noly for commands that match whats currently typed
bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'

# Ignore duplicate commands when writing to history
export HISTCONTROL=erasedups

# Append don't overwrite history
shopt -s histappend

# Defaults in debian so i use them here
HISTSIZE=1000
HISTFILESIZE=2000

# Update line and columns
shopt -s checkwinsize

# ls pretty c:
alias ls='ls --color=auto'
# grep pretty c:
alias grep='grep --color=auto'

# usefull with git and sudo -e
export EDITOR=nvim

#Nice shorthand
alias update="doas pacman -Syu && auracle outdated"

#Enable bin lookup if package not found
source /usr/share/doc/pkgfile/command-not-found.bash

#pfetch
#neofetch
#treefetch
treefetch -xmas
#uwufetch

num_items=$(canto-remote status)
if [ $num_items -gt 0 ]; then
    echo "$num_items unread items!"
fi

auracle outdated
echo
curl sv.wttr.in/Stockholm?0Q 
